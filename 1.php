
<?php

// Create a MySQL Table Using MySQLi
// We will create a table named "products",
// with five columns: "id", "sku", "name", "price", "type description" and "parametrs".
// Example (MySQLi Procedural)

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "demo";

// Create connection
$connection = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$connection){
	die("Connection failed: ".mysqli_connect_error());
}

// Query to create table.
	$query1 = "CREATE TABLE IF NOT EXISTS product(
	ID INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	SKU VARCHAR(255) NOT NULL,
	Name VARCHAR(255) NOT NULL,
	Price VARCHAR(255) NOT NULL,
	Type VARCHAR(255) NOT NULL,
	Attribute VARCHAR(255) NOT NULL
	)";

if (mysqli_query($connection, $query1))
{
	//echo "Table 'products' created successfully.";
} else {
	echo "Error creating table: ".mysqli_error($connection);
}

// Dumping data for table 'product'
$query2 = "INSERT INTO product (sku, name, price, type, attribute) VALUES
('JVC200123', 'Acme DISC', '1.00 $', 'Disc', 'Size: 700 MB'),
('JVC200123', 'Acme DISC', '1.00 $', 'Disc', 'Size: 700 MB'),
('JVC200123', 'Acme DISC', '1.00 $', 'Disc', 'Size: 700 MB'),
('JVC200123', 'Acme DISC', '1.00 $', 'Disc', 'Size: 700 MB'),
('GGWP0007', 'War and Peace', '20.00 $', 'Book', 'Weight: 2 KG'),
('GGWP0007', 'War and Peace', '20.00 $', 'Book', 'Weight: 2 KG'),
('GGWP0007', 'War and Peace', '20.00 $', 'Book', 'Weight: 2 KG'),
('GGWP0007', 'War and Peace', '20.00 $', 'Book', 'Weight: 2 KG'),
('TR120555', 'Chair', '40.00 $', 'Furniture', 'Dimension: 24x45x15'),
('TR120555', 'Chair', '40.00 $', 'Furniture', 'Dimension: 24x45x15'),
('TR120555', 'Chair', '40.00 $', 'Furniture', 'Dimension: 24x45x15'),
('TR120555', 'Chair', '40.00 $', 'Furniture', 'Dimension: 24x45x15')";

if (mysqli_query($connection, $query2))
{
	//echo "New record created successfully.<br>";
} else {
	echo "Error: ".mysqli_error($connection);
}

$result= mysqli_query($connection, "DELETE FROM product WHERE id>12");

// Database connection
// Connect the demo database to the default account:
$connection = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$connection){
	die("Connection failed: ".mysqli_connect_error());
}

// List All Products

$query = "select * from product";
if ($result=mysqli_query($connection, $query))
{
	//echo "All items from 'product' selected successfully.";
} else {
	echo "Error select * from product: ".mysqli_error($connection);
}





// $_GET and $_POST methods

if (isset($_GET['id'])){
	$query = 'delete from product where id='.$_GET['id'];
	if ($result0=mysqli_query($connection, $query)){
		//echo "items deleted from 'product' where id=".$_GET['id']." successfully.";
		} else { echo "Error select * from product: ".mysqli_error($connection);
	}		
}




// The PHP code to process this field

if (isset($_POST['submit'])){

	$selectedOption = $_POST['formActions']; // Store selected value in variable
	$errorMessage = "";

	switch ($selectedOption)
	{
		case "delete" :


		// First demonstration method of the form 
		// with division with checkboxes centered.

		if (isset($_POST['checkbox'])){
			$check = $_POST['checkbox'];
			if (!empty($check))
			{
				foreach ($check as $value){
					$query = "delete from product where id=$value";
					if ($result1=mysqli_query($connection, $query))
					{
						//echo "items from 'product' deleted where id=$value successfully.";
					} else {
						echo "Error select * from product: ".mysqli_error($connection);
					}
				}
				echo "<META HTTP-EQUIV=Refresh CONTENT='1'>";
			}
		}

		// Second method of the form that demonstrates 
		// division of form with checkboxes left floated.
		if (isset($_POST['checkbox2'])){
			$check2 = $_POST['checkbox2'];
			if (!empty($check2)){
				foreach ($check2 as $value){
					$query = "delete from product where id=$value";
					if ($result2=mysqli_query($connection, $query)){
						//echo "items from 'product' deleted where id=$value successfully.";
					} else {
						echo "Error select * from product: ".mysqli_error($connection);
					}
				}
				echo "<META HTTP-EQUIV=Refresh CONTENT='1'>";
			}
		}
		break;

		case addProduct : 
		// Rredirecting to Product Add page. 
		header("Location: 2.php") ;
		break;
	}
}
?>




<!doctype html>

<style>
	#container1{
		display: grid;
		grid-template-rows: 1fr 1fr 1fr;
		grid-template-columns: 1fr 1fr 1fr 1fr;
		grid-gap: 2vw;
	}
	.container{
		float: left;
		border-style: solid;
		text-align: center;
	}
</style>
<html>
<head>
	<title>Product List</title>
</head>
<body>



	<!-- 
	This method draws the table on the page,
	and fills with data from database;
	it should be commented out
	but for testing purposes serves for demonstation purpose as the two other.
	-->

	<table cellpadding="2" cellspacing="2" border="0">
		<tr>
			<th>ID</th>
			<th>SKU</th>
			<th>Name</th>
			<th>Price</th>
			<th>Type</th>
			<th>Attribute</th>
			<th>Delete Action</th>
		</tr>
		<?php while ($product = mysqli_fetch_object($result)) {?>
			<tr>
				<td><?php echo $product->ID; ?></td>
				<td><?php echo $product->SKU; ?></td>
				<td><?php echo $product->Name; ?></td>
				<td><?php echo $product->Price; ?></td>
				<td><?php echo $product->Type; ?></td>
				<td><?php echo $product->Attribute; ?></td>
				<td><a href="1.php?id=<?php echo $product->ID; ?>">Delete</a></td>
			</tr>
		<?php } ?>
	</table>




	<!--
	The first method to draw the form:
	-->

	<form action="" method="post"/>
	<h1>Product List
		<select name="formActions" size="1" style="float: right;">
			<option value="delete">Mass Delete Action</option>
			<option value="addProduct">Add Product (Add/Edit Attributes)</option>
		</select>
		<input type="submit" name="submit" value="Apply" style="float: right;"/>
	</h1>
	<hr>

	<div id="container1">

		<?php
		$query = "SELECT * FROM product";
		if ($result=mysqli_query($connection, $query)){
			//echo "All items from 'product' selected successfully.<br><br>";
		} else {
			echo "Error select * from product: ".mysqli_error($connection);
		}

		while($row = mysqli_fetch_assoc($result)){
			echo "<div class='container'>";

			$id=$row['ID'];
			echo "<input type='checkbox' value='$id' name='checkbox[]' style:'float: left;' />";
			echo "<p>".$row["SKU"]."</p>";
			echo "<p>".$row["Name"]."</p>";
			echo "<p>".$row["Price"]."</p>";
			//echo "<p>".$row["Type"]."</p>";
			echo "<p>".$row["Attribute"]."</p>";
			echo "</div>";
		}  ?>
	</div>


	<br>
	<br>
	<hr>
	<br>
	<br>

	<!--
	The second method to draw the form:
	-->

	<div id="container1">
		<?php

		$products[] = 0;
		$query = "select * from product";
		if ($result=mysqli_query($connection, $query)){
			//echo "All items from 'product' selected successfully.";
		} else {
			echo "Error select * from product: ".mysqli_error($connection);
		}
		if($result) {
			while ($product1 = mysqli_fetch_object($result)){
				$products[] = $product1;
			}
		}

		foreach (array_slice($products, 1) as $product){ ?>
			<div class="container">

				<input 	type="checkbox" value=<?php echo $product->ID; ?>
						name="checkbox2[]" style="float: left;" />
				<p><?php echo  "".$product->SKU;?></p>
				<p><?php echo $product->Name;	?></p>
				<p><?php echo $product->Price;?></p>
				<!--<p><?php echo $product->Type; 	?></p> -->
				<p><?php echo $product->Attribute; ?></p>
			</div>
		<?php } ?>
	</div>

</form>
</body>
</html>


<?php 
mysqli_close($connection);
?>
